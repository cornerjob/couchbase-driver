# couchbase-driver-cornerjob #

A simple driver used to connect to any bucket in a couchbase cluster

## Install ##
```
#!bash
npm install --save couchbase-driver-cornerjob
```

## How to use ##
```
#!javascript
const CouchbaseDriver = require('couchbase-driver-cornerjob');
const host = '12.345.678.910';
const driver = new CouchbaseDriver(host);

driver
  .getBucketConnection('default')
  .then(bucket => {
    return new Promise((resolve, reject) => {
      bucket.get('test', (err, result) => {
        if (err) {
          return reject(err);
        }

        resolve(result);
      });
    });
  })
  .then(() => {
    driver.closeAllBucketConnections();
  })
  .catch(() => {
    driver.closeAllBucketConnections();
  });
```

## More info ##
*getBucketConnection()* returns a couchbase Bucket class from **npm couchbase**.  To learn more about how to perform bucket operations see the [couchbase Node.js SDK](http://docs.couchbase.com/sdk-api/couchbase-node-client-2.3.2/index.html)
