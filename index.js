'use strict';

const { Cluster, N1qlQuery } = require('couchbase-promises');
const logger = require('simple-logger-cornerjob').getLogger('CouchbaseDriver');

const CouchbaseDriver = function (host, cluster, n1qlQuery) {
  this.cluster = cluster || new Cluster(host);
  this.n1qlQuery = n1qlQuery || N1qlQuery;
  this.bucketConnections = {};
};

CouchbaseDriver.prototype.getBucketConnection = function (bucket) {
  if (!this.bucketConnections[bucket]) {
    const self = this;

    return new Promise((resolve, reject) => {
      const connection = self.cluster.openBucket(bucket, err => {
        if (err) {
          logger.error(err);
          return reject(err);
        }

        logger.info(`Connected to ${bucket}`);
        self.bucketConnections[bucket] = connection;
        resolve(connection);
      });
    });
  }

  return Promise.resolve(this.bucketConnections[bucket]);
};

CouchbaseDriver.prototype.closeBucketConnection = function (bucket) {
  if (!this.bucketConnections[bucket]) {
    return logger.error(`${bucket} connection not found`);
  }

  try {
    const connection = this.bucketConnections[bucket];
    connection.disconnect();
    logger.info(`${bucket} closed`);
  }
  catch (err) {
    logger.error(err);
  }
};

CouchbaseDriver.prototype.closeAllBucketConnections = function () {
  let connection;

  for (const bucket in this.bucketConnections) {
    try {
      connection = this.bucketConnections[bucket];
      connection.disconnect();
      logger.info(`${bucket} closed`);
    }
    catch (err) {
      logger.error(err);
    }
  }
};

module.exports = CouchbaseDriver;
